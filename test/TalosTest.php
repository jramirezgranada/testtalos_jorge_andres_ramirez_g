<?php

require_once ("Talos.php");

class TalosTest extends \PHPUnit_Framework_TestCase
{
    private $talos;

    public function __construct(){
        $this->talos = new Talos();
    }
    /**
    * Create an array of N generated words.
    *   - Words have to be generated.
    *   - Words have to be unique.
    *   - Words could not contain numbers.
    *
    * Example:
    *   Input: 100
    *   Output:
    *     ['Word', 'letter', 'asd', ... ]
    */
    public function testTalos1()
    {
        $words = $this->talos->talos1(100);

        //array of 100 generated words
        $this->assertEquals(100, count($words));
        
        //Words have to be unique
        $wordsArray = array_count_values($words);

        foreach ($wordsArray as $wordKey => $wordValue) {
            $this->assertEquals(1, $wordValue);
        }

        //Words could not contain numbers
        
        foreach ($words as $word) {
            $this->assertInternalType('string', $word);
        }

        print_r($words);

    }

    /**
    * Create a proportional matrix like the following (m X m dimensions),
    * with generated values.
    * - Assert first row sum is 3
    * - Assert horizontal sum rows is 9
    * - Assert last column sum is 6
    *
    * Example
    *  Input: 3
    *  Output:
    *   [
    *     [1, 0, 2],
    *     [1, 0, 2],
    *     [1, 0, 2]
    *   ]
    */
    public function testTalos2()
    {
        $array = $this->talos->talos2(3);

        //Assert type array
        $this->assertInternalType('array', $array);
        
        //Assert first row sum is 3
        $this->assertEquals(3, array_sum($array[0]));

        //Assert horizontal sum rows is 9
        $horSumRow = 0;

        for ($i=0; $i < count($array) ; $i++) { 
            $horSumRow += array_sum($array[$i]);
        }

        $this->assertEquals(9, $horSumRow);
        
        //Assert last column sum is 6
        $lastColSum = 0;

        for ($i=0; $i < count($array) ; $i++) { 
            $lastColSum += $array[$i][count($array) - 1];
        }

        $this->assertEquals(6, $lastColSum);

        print_r($array);

    }

    /**
  	* During a long process we want to show the user a progress percentage.
    * You will have to create an array with every 1% progress with the right proportion number.
  	* The inputs are two long integer numbers.
  	*
  	* Example:
  	*  Input:
    *   2 to 200
    *   proportion: 2
    *   total: 200
  	*  output:
    *  		[
    *  	    0 => 0,
    *  			1 => 2,
    *  			2 => 4,
    *  			...
    *  			100 => 200
    *     ]
    */
    public function testTalos3()
    {
        $total = 200;
        $proportion = 2;

        $percArray = $this->talos->talos3($total);
        // Assert array with proportion numbers

        $this->assertInternalType('array', $percArray);

        for ($i=1; $i < count($percArray) ; $i++) { 
            if($percArray[$i] == 0){
                $this->assertEquals(0,0);
            }

            $prop = $percArray[$i+1] - $percArray[$i];

            $this->assertEquals($proportion, $prop);
        }
        
        print_r($percArray);
        //Other cases

    }
}
