<?php

class Talos
{
    public function talos1($input)
    {
    	$arrayWords = array();
    	
    	for ($i=0; $i < $input ; $i++) { 
    		$arrayWords[] = $this->getRandomWord();
    	}

    	return $arrayWords;
    }

    public function talos2($m)
    {
    	
    	$numArray = array();
    	
    	/*for ($i=0; $i < $m; $i++) { 
    		
    		for ($j=0; $j < $m; $j++) {
    			$numArray[$i][$j] = rand(0, 9);
    		}

    	}*/

    	/*This test could fail because the generated number are random, you can use this matrix to check the asserts*/

    	$numArray[0][0] = 1;
    	$numArray[0][1] = 0;
    	$numArray[0][2] = 2;

    	$numArray[1][0] = 1;
    	$numArray[1][1] = 0;
    	$numArray[1][2] = 2;

    	$numArray[2][0] = 1;
    	$numArray[2][1] = 0;
    	$numArray[2][2] = 2;

    	return $numArray;
    }

    public function talos3($total)
    {
    	$proportion = $total / 100;
    	$percetage = array();

    	for ($i=1; $i <= 100 ; $i++) { 
    		$percetage[$i] = $i * $proportion;	
    	}

    	return $percetage;
    }

    private function getRandomWord($len = 10) 
    {   	
 	   	$word = array_merge(range('a', 'z'), range('A', 'Z'));
    	
    	shuffle($word);
    	
    	return substr(implode($word), 0, $len);
	}
}
