#Instructions
  - Please read the instructions for each test in the file test/TalosTest.php
  - Create the test functions for each description in test/TalosTest.php
  - Create the functions for each test in the file Talos.php
  - Include your name and last name in the project folder Example: testTalos__Name_LastName

#Install phpunit
  composer install

#Run unitary tests
  vendor/bin/phpunit test
